
var HttpComponent = require('elasticio-node').HttpComponent;

const messages = require('elasticio-node').messages;

// exporting the process function to be called by elastic.io runtime
exports.process = doProcess;

function doProcess(msg, cfg) {

    // creating requestion options
    var options = {
        url: 'https://swapi.co/api/planets/2/',
        json: true
    };

    // sending GET request with given options
    new HttpComponent(this).get(options);


    // const body = {
    //     name: 'Alderaan',
    //     rotation_periode: "24",
    //     orbital_periode: "364",
    //     diameter: "12500",
    // };

    // this.logger.info('*** planet name: %s', body.name);
    // this.logger.info('*** planet diameter: %s', body.diameter);

    // this.emit('data', messages.newMessageWithBody(body));
    // this.emit('end');
}
