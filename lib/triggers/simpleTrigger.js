const messages = require('elasticio-node').messages;

const randomQuote = require('random-quotes');

async function simpleTrigger(msg, conf, snapshot = {}) {

    const now = new Date().getTime();

    const newQuote = randomQuote.default();

    const lastPoll = snapshot.lastPoll || new Date(0).toISOString();

    this.logger.info('Last polling timestamp=%s', lastPoll);

    const newSnapshot = {
        lastPoll: new Date(now).toISOString()
    };

    const body = {
        fireTime: new Date(now),
        lastPoll,
        quote: newQuote.body,
        author: newQuote.author
    };

    this.logger.info('*** quote: %s', newQuote.body);
    this.logger.info('*** author: %s', newQuote.author);

    this.emit('data', messages.newMessageWithBody(body));
    this.emit('snapshot', newSnapshot);

    this.emit('end');
}

exports.process = simpleTrigger;
